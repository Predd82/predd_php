<?php
include('header.php');
?>

    <p>ЛР 11. Выбрать три разные точки заданного на плоскости множества точек, составляющие треугольник наибольшего периметра.</p>
    <form name="form" method="get">
        <p>введите количество точек:</p>
        <label>
            <input type="number" name="lim" size="40" placeholder="лимит множества" value="<?php echo $_GET['lim'];?>">
        </label><br/><br/>
        <input type="submit" value="посчитать">
    </form>
    <hr>
<?php
/**
 * Created by PhpStorm.
 * User: Predd
 * Date: 30.05.2016
 * Time: 14:28
 */
if(isset($_GET['lim'])){                                                                         //если поле заполнено
    $lim = $_GET['lim'];                                         //то переменной $string  присвоить введенное значение
}

$x=array();
$y=array();
$result=array();

echo 'множество точек:<br/>';

for($i=0;$i<$lim;$i++){
    $x[$i]=rand(0,10);
    $y[$i]=rand(0,10);
    echo ($i+1).". $x[$i]:$y[$i]"."<br/>";//рандомим точки
}

asort($x);//
$start_keys=array_keys($x);//ищем крайнюю точку отсортировав массив и взяв первое значение
$start_x= $start_keys[0];

    for($i=0;$i<$lim;$i++) {
        $result[$i] = sqrt((pow(($x[$start_x] - $x[$i]), 2) + pow(($y[$start_x] - $y[$i]), 2)));//измеряем длины от крайней точки до всех остальных
    }

arsort($result);//сортируем по убыванию
$keys=array_keys($result);//получаем индексы максимальных значений

$point1=array_search(max($result),$result); //ищем индекс макс значения (это самая дальняя точка)
$point2=$keys[1];//берем индекс второго по длине отрезка
$point3=array_search(0,$result);// индекс стартовой точка

echo "Ответ: точки с координатами  $x[$point1]:$y[$point1]; $x[$point2]:$y[$point2]; $x[$point3]:$y[$point3];";

echo "<pre>";
//var_dump($result);//дебаг
echo "</pre>";