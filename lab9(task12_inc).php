<?php
include('header.php');
?>
<hr><p>ЛР №9. Найти и напечатать, сколько раз повторяется в тексте каждое слово.</p>
<form name="form" method="get">
    <p>введите текст и два слова:</p>
    <label>
        <input type="TEXT" name="string" size="100" placeholder="введите текст" value="<?php echo $_GET['string']?>">
    </label><br/><br/>
    <input type="submit" value="Подсчитать" name="go">
</form>
<br/>

<?php
if(isset($_GET['string'])){
    $string=$_GET['string'];
} else $string='Введите фразу!';//устанавливаем переменную string

echo "$string<br/><br/>";//выводим исходный текст
$string = preg_replace('/[^\w\s]/u', ' ', $string);//удаляем знаки препинания
$string = mb_strtolower($string,'UTF-8');          //переводим строку в нижний регистр
$words = explode (' ',$string);                    //разбиваем строку на массив из слов
$count = array_count_values($words);               //Подсчитывает количество всех разных значений массива  и создает массив[значение(слово)]->количество (это количество слов)
arsort($count);                                    //сортируем массив [слово]->количество по убыванию количества - опция
$words = array_keys($count);                       //создаем массив из ключей ассоциативного массива (это наши слова)
$i=0;                       //счетчик цикла
foreach($count as $a) {                     //цикл перебор массива по очереди
    echo $words[$i].' - '.$a.' раз(а)<br/>';//выводим элемент массива $words[i] и текущее значение массива-счетчика
    $i++;                  //счетчик цикла+1
}                          //и так до конца массива :)

?>
<?php
include('footer.php');
?>