<?php
include('header.php');
?>

    <p>ЛР 12. Найти такую точку заданного на плоскости множества точек, сумма расстояний от которой до остальных минимальна.</p>
    <form name="form" method="get">
        <p>введите количество точек:</p>
        <label>
            <input type="number" name="lim" size="40" placeholder="лимит множества" value="<?php echo $_GET['lim'];?>">
        </label><br/><br/>
        <input type="submit" value="посчитать">
    </form>
    <hr>

<?php
/**
 * Created by PhpStorm.
 * User: Predd
 * Date: 30.05.2016
 * Time: 21:35
 */
if(isset($_GET['lim'])){                                                                         //если поле заполнено
    $lim = $_GET['lim'];                                         //то переменной $string  присвоить введенное значение
}

$x=array();
$y=array();
$length=array();
$res=array();

echo 'множество точек:<br/>';

for($i=0;$i<$lim;$i++){
    $x[$i]=rand(-100,100);
    $y[$i]=rand(-100,100);
    echo ($i+1).". $x[$i]:$y[$i]"."<br/>";//рандомим точки
}

echo "Сумма длин до остальных точек: <br/>";

for($a=0;$a<$lim;$a++){
    for ($i = 0; $i < $lim; $i++) {//перебираем все точки
        $result[$i] = sqrt((pow(($x[$a] - $x[$i]), 2) + pow(($y[$a] - $y[$i]), 2)));//измеряем длины от точки "а" до всех остальных
    }
    $length = array_sum($result);
    $res[$a]=$length;
}
foreach ($res as $t ) {
    $c++;
    echo $c.' '.$t."<br/>";//на экран сумму значений для каждой точки
}

$key= array_search(min($res),$res);//ищем индекс минимального значения

echo "<br/>Ответ: от точки ".($key+1)." сумма расстояний до остальных минимальна";
?>
